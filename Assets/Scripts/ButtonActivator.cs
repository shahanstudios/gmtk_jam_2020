﻿using UnityEngine;

[RequireComponent(typeof(Activator))]
public class ButtonActivator : MonoBehaviour {
	public bool isEntityAbovePreTick = false;
	public bool isEntityAbovePostTick = false;

	public bool isPressed => !isEntityAbovePreTick && isEntityAbovePostTick;
	public bool isDepressed => isEntityAbovePreTick && !isEntityAbovePostTick;

	private Activator activator;

	private void Awake() {
		activator = GetComponent<Activator>();
	}

	private void OnEnable() {
		LevelManager.Instance.OnLevelTick += ProcessEntities;
		LevelManager.Instance.OnLevelPostTick += MoveEntities;
	}
	private void OnDisable() {
		LevelManager.Instance.OnLevelTick -= ProcessEntities;
		LevelManager.Instance.OnLevelPostTick -= MoveEntities;
	}
	
	private void ProcessEntities() {
		isEntityAbovePreTick = false;
		foreach (var entity in LevelManager.Instance.Entities) {
			if (entity != this.gameObject && entity.transform.position.XZ() == this.transform.position.XZ()) {
				isEntityAbovePreTick = true;
				break;
			}
		}
	}

	private void MoveEntities() {
		isEntityAbovePostTick = false;
		foreach (var entity in LevelManager.Instance.Entities) {
			if (entity != this.gameObject && entity.transform.position.XZ() == this.transform.position.XZ()) {
				isEntityAbovePostTick = true;
				break;
			}
		}

		if (isPressed) {
			activator.FireActiveEvent();
		}
		else if (isDepressed) {
			activator.FireDeactiveEvent();
		}
	}
}
