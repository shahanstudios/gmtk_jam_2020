﻿using UnityEngine;

[RequireComponent(typeof(Activator))]
public class Wire : MonoBehaviour {
	[SerializeField]
	private Color activeColor = Color.red;
	[SerializeField]
	private Color inactiveColor = Color.gray;

	private new MeshRenderer renderer;

	private MaterialPropertyBlock mpb;
	private MaterialPropertyBlock Mpb {
		get {
			if (mpb == null) {
				mpb = new MaterialPropertyBlock();
			}
			return mpb;
		}
	}

	private Activator activator;
	private void Awake() {
		activator = GetComponent<Activator>();
	}

	private void Start() {
		renderer = GetComponentInChildren<MeshRenderer>();


		activator.OnActivationEvent += () => {
			UpdateColor(activeColor);
		};
		activator.OnDeactivationEvent += () => {
			UpdateColor(inactiveColor);
		};

		activator.FireDeactiveEvent();
	}
	private void UpdateColor(Color color) {
		int id = 0;
		if (activator.upN) {
			id += 4;
		}
		if (activator.downN) {
			id += 8;
		}
		if (activator.leftN) {
			id += 1;
		}
		if (activator.rightN) {
			id += 2;
		}
		Mpb.SetInt("_ID", id);
		Mpb.SetColor("_BaseColor", color);
		renderer.SetPropertyBlock(Mpb);
	}
}
