﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour {
	public string level;
	public void LoadLevel() {
		SceneManager.LoadScene(level);		
	}
}
