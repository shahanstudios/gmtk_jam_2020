﻿using UnityEngine;

public class ActorMovement : MonoBehaviour {
	private new Transform transform;
	private Transform meshTransform;

	private void Awake() {
		transform = GetComponent<Transform>();	
		meshTransform = transform.GetChild(0).transform;
	}

	public bool Move(Vector2 movement) {
		if (movement.sqrMagnitude == 1) {
			Vector2Int newPos = new Vector2Int((int)transform.position.x + (int)movement.x, (int)transform.position.z + (int)movement.y);

			TileData newTile = LevelManager.Instance.Tiles[newPos.x, newPos.y];
			meshTransform.rotation = Quaternion.Euler(90, (movement.x < 0 ? -1 : 1) * Mathf.Acos(Vector2.Dot(Vector3.up, movement)) * Mathf.Rad2Deg, 0);
			if (newTile.isEmpty) {
				bool clearPath = true;
				foreach (var entity in LevelManager.Instance.Entities) {
					if (entity != this.gameObject && entity != LevelManager.Instance.player && entity.transform.position.XZ() == newPos && entity.TryGetComponent(out ActorMovement mover)) {
						if (!mover.Move(movement)) {
							clearPath = false;
							break;
						}
					}
				}
				if (clearPath) {
					transform.Translate(new Vector3(movement.x, 0, movement.y), Space.World);
					return true;
				}
				
			}
		}
		return false;
	}
}
