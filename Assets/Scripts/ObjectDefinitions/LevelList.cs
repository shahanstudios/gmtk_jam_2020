﻿using UnityEngine;

[CreateAssetMenu()]
public class LevelList : ScriptableObject {
	[SerializeField]
	private LevelStructure[] levels;

	public int Count => levels.Length;

	public LevelStructure this[int i] => GetLevel(i);

	public LevelStructure GetLevel(int index) {
		if (index >= 0 && index < levels.Length) {
			return levels[index];
		}
		return null;
	}

	public int GetLevelIndex(LevelStructure level) {
		return System.Array.IndexOf(levels, level);
	}
	public LevelStructure GetNextLevel(LevelStructure level) {
		return GetLevel(GetLevelIndex(level) + 1);
	}
}
