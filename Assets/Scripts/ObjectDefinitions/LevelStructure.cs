﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Point = UnityEngine.Vector2Int;

[CreateAssetMenu]
public class LevelStructure : ScriptableObject {
	[SerializeField]
	private Texture2D levelTexture = null;
	public string Name;
	private int width;
	private int height;
	private Point startingLocation;
	private Point goalLocation;

	[SerializeField]
	private List<EntityDefinition> entityDefinitions = new List<EntityDefinition>();
	private Dictionary<Color, List<GameObject>> EntityDefinitions => entityDefinitions.ToDictionary(def => def.color, def => def.entityPrefab);

	public int Width => width;
	public int Height => height;
	public Point StartingLocation => startingLocation;
	public Point GoalLocation => goalLocation;

	private Dictionary<Point, List<GameObject>> positionEntityMap;
	public Dictionary<Point, List<GameObject>> PositionEntityMap => positionEntityMap;

	public TileData[,] GetTileData() {
		width = levelTexture.width; height = levelTexture.height;
		Color[] pixels = levelTexture.GetPixels();

		var defs = EntityDefinitions;
		positionEntityMap = new Dictionary<Point, List<GameObject>>();

		TileData[,] data = new TileData[width, height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				Color pixel = pixels[x + y * width];
				data[x, y] = new TileData(pixel != Color.black);
				if (pixel == Color.green) {
					startingLocation = new Point(x, y);
				}
				else if (pixel == Color.red) {
					goalLocation = new Point(x, y);
				}
				else if (defs.ContainsKey(pixel)) {
					positionEntityMap.Add(new Point(x, y), defs[pixel]);
				}
			}
		}

		return data;
	}
}

[System.Serializable]
public struct TileData {
	public readonly bool isEmpty;
	public TileData(bool isEmpty = true) {
		this.isEmpty = isEmpty;
	}
}
[System.Serializable]
public struct EntityDefinition {
	public Color color;
	public List<GameObject> entityPrefab;
}
