﻿using System.Linq;
using UnityEngine;

public class Activator : MonoBehaviour {
	public bool canActivate = true;
	public bool invert = false;
	public System.Type activatorFilter = null;

	public Activator upN;
	public Activator downN;
	public Activator leftN;
	public Activator rightN;

	public bool _isActive = false;
	public bool isActive => (invert) ? !_isActive : _isActive;

	public event System.Action OnActivationEvent;
	public event System.Action OnDeactivationEvent;

	private void Start() {
		OnActivationEvent += () => {
			this._isActive = true;
			if (!canActivate) {
				return;
			}
			if (upN && upN._isActive != this.isActive) {
				upN.FireActiveEvent();
			}
			if (downN && downN._isActive != this.isActive) {
				downN.FireActiveEvent();
			}
			if (leftN && leftN._isActive != this.isActive) {
				leftN.FireActiveEvent();
			}
			if (rightN && rightN._isActive != this.isActive) {
				rightN.FireActiveEvent();
			}
		};
		
		OnDeactivationEvent += () => {
			this._isActive = false;
			if (!canActivate) {
				return;
			}
			if (upN && upN._isActive != this.isActive) {
				upN.FireDeactiveEvent();
			}
			if (downN && downN._isActive != this.isActive) {
				downN.FireDeactiveEvent();
			}
			if (leftN && leftN._isActive != this.isActive) {
				leftN.FireDeactiveEvent();
			}
			if (rightN && rightN._isActive != this.isActive) {
				rightN.FireDeactiveEvent();
			}
		};

		if (_isActive) {
			FireActiveEvent();
		}
		else {
			FireDeactiveEvent();
		}
		if (!canActivate) {
			return;
		}
		Activator[] wires = GameObject.FindObjectsOfType<Activator>();
		upN = wires.Where(w => (activatorFilter == null || w.GetComponent(activatorFilter) != null) && (w.transform.position.XZ() == transform.position.XZ() + Vector2.up)).FirstOrDefault();
		downN = wires.Where(w => (activatorFilter == null || w.GetComponent(activatorFilter) != null) && (w.transform.position.XZ() == transform.position.XZ() + Vector2.down)).FirstOrDefault();
		leftN = wires.Where(w => (activatorFilter == null || w.GetComponent(activatorFilter) != null) && (w.transform.position.XZ() == transform.position.XZ() + Vector2.left)).FirstOrDefault();
		rightN = wires.Where(w => (activatorFilter == null || w.GetComponent(activatorFilter) != null) && (w.transform.position.XZ() == transform.position.XZ() + Vector2.right)).FirstOrDefault();

	}

	public void FireActiveEvent() {
		OnActivationEvent?.Invoke();	
	}
	public void FireDeactiveEvent() {
		OnDeactivationEvent?.Invoke();	
	}
}
