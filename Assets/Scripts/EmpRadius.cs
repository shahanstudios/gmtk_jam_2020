﻿using UnityEngine;

[RequireComponent(typeof(Activator))]
public class EmpRadius : MonoBehaviour {
	public int radius = 3;
	public MeshRenderer radiusRenderer;

	private Activator activator;
	public bool isActive => activator.isActive;
	private void Start() {
		activator = GetComponent<Activator>();
		activator.activatorFilter = typeof(EmpRadius);
		activator.OnActivationEvent += () => {
			radiusRenderer.enabled = activator.isActive;
		};
		activator.OnDeactivationEvent += () => {
			radiusRenderer.enabled = activator.isActive;
		};
	}
	
	public bool ContainsPoint(Vector2Int point) {
		int dist = Mathf.FloorToInt(Mathf.Abs(point.x - transform.position.x) + Mathf.Abs(point.y - transform.position.z));
		return dist <= radius - 1;
	}

	private void OnDrawGizmos() {
		Gizmos.DrawWireSphere(transform.position, radius);
	}
}
