﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Activator))]
public class ConveyerBelt : MonoBehaviour {
	[SerializeField]
	private Color activeColor = Color.red;
	[SerializeField]
	private Color inactiveColor = Color.gray;

	private List<GameObject> gameObjectsToMove; 

	private new MeshRenderer renderer;

	private MaterialPropertyBlock mpb;
	private MaterialPropertyBlock Mpb {
		get {
			if (mpb == null) {
				mpb = new MaterialPropertyBlock();
			}
			return mpb;
		}
	}

	private Activator activator;
	private void Awake() {
		activator = GetComponent<Activator>();
		activator.activatorFilter = typeof(ConveyerBelt);
	}
	private void Start() {
		renderer = GetComponentInChildren<MeshRenderer>();
		activator.OnActivationEvent += () => {
			Mpb.SetColor("_BaseColor", activeColor);
			renderer.SetPropertyBlock(Mpb);
		};
		activator.OnDeactivationEvent += () => {
			Mpb.SetColor("_BaseColor", inactiveColor);
			renderer.SetPropertyBlock(Mpb);
		};

		if (activator.isActive) {
			activator.FireActiveEvent();
		}
		else {
			activator.FireDeactiveEvent();
		}
	}

	private void OnEnable() {
		LevelManager.Instance.OnLevelTick += ProcessEntities;
		LevelManager.Instance.OnLevelPostTick += MoveEntities;
	}
	private void OnDisable() {
		LevelManager.Instance.OnLevelTick -= ProcessEntities;
		LevelManager.Instance.OnLevelPostTick -= MoveEntities;
	}
	
	private void ProcessEntities() {
		if (!activator.isActive) {
			return;
		}
		gameObjectsToMove = new List<GameObject>();
		foreach (var entity in LevelManager.Instance.Entities) {
			if (entity != this.gameObject && entity.transform.position.XZ() == this.transform.position.XZ()) {
				gameObjectsToMove.Add(entity);
			}
		}
	}

	private void MoveEntities() {
		if (!activator.isActive) {
			return;
		}
		foreach (var entity in gameObjectsToMove) {
			if (entity.transform.position.XZ() == this.transform.position.XZ()) {
				entity.transform.Translate(transform.forward);
			}
		}
	}
}
