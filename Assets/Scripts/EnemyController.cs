﻿using UnityEngine;

[RequireComponent(typeof(ActorMovement))]
public class EnemyController : MonoBehaviour {
	private ActorMovement enemyMovement;

	private void Awake() {
		enemyMovement = GetComponent<ActorMovement>();
	}
	private void Start() {
		LevelManager.Instance.OnLevelTick += Move;
	}

	private void Move() {
		enemyMovement.Move(Vector2.right);
	}
}
