﻿using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(ActorMovement))]
public class PlayerController : MonoBehaviour {
	private ActorMovement playerMovement;
	private PlayerInput playerInput;

	private void Awake() {
		playerMovement = GetComponent<ActorMovement>();
		playerInput = new PlayerInput();
	}
	private void OnEnable() {
		playerInput.Enable();
		playerInput.Movement.Move.performed += Move;
		playerInput.Movement.Wait.performed += Wait;
	}
	private void OnDisable() {
		playerInput.Disable();
		playerInput.Movement.Move.performed -= Move;
		playerInput.Movement.Wait.performed -= Wait;
	}

	private void Move(InputAction.CallbackContext context) {
		EmpRadius[] emps = GameObject.FindObjectsOfType<EmpRadius>();
		bool isInEmp = false;
		for (int i = 0; i < emps.Length; i++) {
			if (emps[i].isActive && emps[i].ContainsPoint(new Vector2Int((int)transform.position.x, (int)transform.position.z))) {
				isInEmp = true;
				break;
			}
		}
		if (!isInEmp) {
			LevelManager.Instance.TickLevel();
			Move(context.ReadValue<Vector2>());
			LevelManager.Instance.PostTickLevel();
		}

	}
	public void Move(Vector2 movement) {
		Vector2Int newPos = new Vector2Int((int)transform.position.x + (int)movement.x, (int)transform.position.z + (int)movement.y);
		bool successful = playerMovement.Move(movement);
		if (successful && LevelManager.Instance.GoalLocation == newPos) {
			LevelManager.Instance.CompleteLevel();
		}
	}
	private void Wait(InputAction.CallbackContext context) {
		LevelManager.Instance.TickLevel();
		LevelManager.Instance.PostTickLevel();
	}
}
