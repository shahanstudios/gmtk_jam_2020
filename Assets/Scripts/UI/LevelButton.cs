﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

[RequireComponent(typeof(Button))]
public class LevelButton : MonoBehaviour {
	public LevelStructure levelStructure;
	
	private TextMeshProUGUI levelText;
	private Button button;
	
	private void Awake() {
		levelText = GetComponentInChildren<TextMeshProUGUI>();
		button = GetComponentInChildren<Button>();

		button.onClick.AddListener(LoadScene);
	}
	private void Start() {
		if (levelText != null && levelStructure != null) {
			levelText.text = $"{levelStructure.Name}";
		}
	}

	private void LoadScene() {
		System.Collections.IEnumerator load() {
			SceneManager.LoadScene(2, LoadSceneMode.Additive);
			yield return new WaitForSeconds(0.3f);
			LevelManager.Instance.levelStructure = levelStructure;
			LevelManager.Instance.InstantiateLevel();
			SceneManager.UnloadScene(1);
			LevelManager.Instance.PolishUpdate();
		}
		StartCoroutine(load());
	}
}
