﻿using UnityEngine;

public class LevelGrid : MonoBehaviour {
	[SerializeField]
	private LevelList levelList;
	[SerializeField]
	private GameObject levelButtonPrefab;

	private void Start() {
		for (int i = 0; i < levelList.Count; i++) {
			GameObject btn = GameObject.Instantiate(levelButtonPrefab);
			btn.transform.SetParent(this.transform);
			btn.GetComponent<LevelButton>().levelStructure = levelList[i];
		}
	}
}
