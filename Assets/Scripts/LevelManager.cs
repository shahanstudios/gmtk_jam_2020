﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
	private static LevelManager instance;
	public static LevelManager Instance => instance;

	public LevelStructure levelStructure = null;
	[SerializeField]
	private GameObject playerPrefab = null;
	[SerializeField]
	private GameObject goalPrefab = null;
	[SerializeField]
	private GameObject obstaclePrefab = null;
	[SerializeField]
	private GameObject floorPrefab = null;

	private TileData[,] tiles;

	public TileData[,] Tiles => tiles;
	public Vector2Int GoalLocation => levelStructure.GoalLocation;

	public event Action OnLevelTick;
	public event Action OnLevelPostTick;
	public event Action OnLevelCompletedEvent;

	public List<GameObject> Entities = new List<GameObject>();
	public GameObject player;

	private PlayerInput playerInput;

	private void Awake() {
		instance = this;
		playerInput = new PlayerInput();
	}
	private void Start() {
		OnLevelCompletedEvent += () => {
			SceneManager.LoadScene(1);
		};
		
		InstantiateLevel();
	}

	private void OnEnable() {
		playerInput.Enable();
		playerInput.Control.ResetLevel.performed += ResetLevel;
		playerInput.Control.ToLevelSelect.performed += ToLevelSelect;
	}
	private void OnDisable() {
		playerInput.Disable();
		playerInput.Control.ResetLevel.performed -= ResetLevel;
		playerInput.Control.ToLevelSelect.performed -= ToLevelSelect;
	}

	public void PolishUpdate() {
		Camera cam = FindObjectOfType<Camera>();
		cam.transform.position = new Vector3(levelStructure.Width / 2f, cam.transform.position.y, levelStructure.Height / 2f);
	}

	public void InstantiateLevel() {
		if (!levelStructure) {
			return;
		}

		GameObject obstacleHolderObj = new GameObject();
		obstacleHolderObj.name = "Obstacle Holder";
		obstacleHolderObj.transform.parent = this.transform;

		tiles = levelStructure.GetTileData();
		for (int y = 0; y < levelStructure.Height; y++) {		
			GameObject rowObject = new GameObject();
			rowObject.name = $"Row {y}";
			rowObject.transform.parent = obstacleHolderObj.transform;
			for (int x = 0; x < levelStructure.Width; x++) {		
				if (!tiles[x,y].isEmpty) {
					GameObject tileObj = GameObject.Instantiate(obstaclePrefab, new Vector3(x, 0, y), Quaternion.identity);
					tileObj.name = $"Tile Obstacle {x}, {y}";
					tileObj.transform.parent = rowObject.transform;
				}
			}
		}

		Entities = new List<GameObject>();
		GameObject entityHolderObj = new GameObject();
		entityHolderObj.name = "Entity Holder";
		entityHolderObj.transform.parent = this.transform;

		player = GameObject.Instantiate(playerPrefab, new Vector3(levelStructure.StartingLocation.x, 0, levelStructure.StartingLocation.y), Quaternion.identity);
		player.transform.parent = entityHolderObj.transform;
		Entities.Add(player);

		GameObject.Instantiate(goalPrefab, new Vector3(levelStructure.GoalLocation.x, 0, levelStructure.GoalLocation.y), Quaternion.identity).transform.parent = this.transform;

		GameObject floor = GameObject.Instantiate(floorPrefab, Vector3.down, Quaternion.identity);
		floor.transform.parent = this.transform;
		floor.transform.localScale = new Vector3(levelStructure.Width, 1, levelStructure.Height);

		foreach (var entry in levelStructure.PositionEntityMap) {
			Vector3 pos = new Vector3(entry.Key.x, 0, entry.Key.y);
			foreach (GameObject p in entry.Value) {

				GameObject entity = GameObject.Instantiate(p, new Vector3(entry.Key.x, 0, entry.Key.y), p.transform.rotation);
				entity.transform.parent = entityHolderObj.transform;
				Entities.Add(entity);
			}
		}
	}
	public void CompleteLevel() {
		OnLevelCompletedEvent?.Invoke();
	}
	private void ResetLevel(InputAction.CallbackContext context) {
		ResetLevel();
	}
	public void ResetLevel() {
		foreach (Transform child in this.transform) {
			Destroy(child.gameObject);
		}
		InstantiateLevel();	
	}
	private void ToLevelSelect(InputAction.CallbackContext context) {
		SceneManager.LoadScene(1);
	}

	public void TickLevel() {
		OnLevelTick?.Invoke();
	}
	public void PostTickLevel() {
		OnLevelPostTick?.Invoke();
	}
}
